package com.company;

import javax.swing.JOptionPane;

public class Menu {
    public String tanggal, nama;
    public String ruang;
    public int jam, jenis, bayar, nomer;
    public int pil;

    //Menu Awal
    public void Pilih(int i) {
        String input = JOptionPane.showInputDialog("Menu Reservasi Raungan Karaoke \n"
                + "1. Reservasi Ruangan \n"
                + "2. Cetak Struk Pembayaran \n"
                + "3. Keluar");
        pil = Integer.valueOf(input);
    }

    //Input dari Keyboard
    public void masukkan() {
        String inputNama = JOptionPane.showInputDialog(null, "Masukkan Nama : ");
        nama = inputNama;
        String inputNo = JOptionPane.showInputDialog(null, "Masukkan No : ");
        nomer = Integer.parseInt(inputNo);
        String inputTgl = JOptionPane.showInputDialog(null, "Masukkan Tanggal : ");
        tanggal = inputTgl;
        String inputJam = JOptionPane.showInputDialog(null, "Masukkan Jam : ");
        jam = Integer.parseInt(inputJam);
        String inputJenis = JOptionPane.showInputDialog(null, "Jenis Ruangan \n"
                + "1. Small Room \n"
                + "2. Medium Room \n"
                + "3. Large Room \n"
                + "4. VIP Room \n"
                + "Pilih Jenis Ruangan : ");
        jenis = Integer.parseInt(inputJenis);
    }

    //Menghitung Harga Ruangan
    public int prosess(int n) {
        if (jenis == 1) {
            ruang = "Small Room";
            n = 72000;
            bayar = jam * n;
        } else if (jenis == 2) {
            ruang = "Medium Room";
            n = 83000;
            bayar = jam * n;
        } else if (jenis == 3) {
            ruang = "Large Room";
            n = 100000;
            bayar = jam * n;
        } else if (jenis == 4) {
            ruang = "VIP Room";
            n = 200000;
            bayar = jam * n;
        } else {
            JOptionPane.showInputDialog(null, "ERROR 404 NOT FOUND");
        }
        return n;
    }

    //Memberi nilai balik (Bayar)
    public int nilaiBalik() {
        return bayar;
    }

    //Mencetak Struk
    public void cetak() {
        JOptionPane.showInputDialog(null,
                "           KARAOKE NOVITA \n"
                        + "Nama             : " +nama+
                        "\nNo Hp            : " +nomer+
                        "\nTanggal          : " +tanggal+
                        "\nJam Sewa         : " +jam+
                        "\nRuangan          : "+ruang+
                        "\nTotal Pembayaran : " +bayar);

        System.out.println("       KARAOKE NOVITA");
        System.out.println("Nama             : "+nama);
        System.out.println("No Hp            : "+nomer);
        System.out.println("Tanggal          : "+tanggal);
        System.out.println("Jam Sewa         : "+jam);
        System.out.println("Ruaungan         : "+ruang);
        System.out.println("Total Pembayaran : "+bayar);
    }

    //Untuk melakukan keluar dari menu
    public void Keluar() {
        int confirm = JOptionPane.showConfirmDialog(null, "Anda Akan Keluar", "Confirm", JOptionPane.YES_NO_OPTION);
        if (confirm == 0) {
            System.exit(0);
        }
    }
}
